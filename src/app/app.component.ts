import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators, MaxLengthValidator } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  forma: FormGroup;
  selector:any;

  constructor(){
    this.forma = new FormGroup({
      'nombre'    : new FormControl(''),
      'apellido'  : new FormControl(''),
      'edad'      : new FormControl(''),
      'selector'  : new FormControl('',[Validators.required])
    });

    console.log('forma_antes', this.forma);

  }

  logic(){

    // this.forma.
    if ( this.forma.get('selector').value == 'nombre' ){
      this.forma.get('nombre').setValidators([Validators.required]);
      this.forma.get('apellido').setValidators([Validators.required]);

      this.forma.get('nombre').updateValueAndValidity();
      this.forma.get('apellido').updateValueAndValidity();

      this.forma.get('edad').clearValidators();
      this.forma.get('edad').updateValueAndValidity();
    }
    else if (this.forma.get('selector').value == 'edad' ){
      this.forma.get('edad').setValidators([Validators.required]);
      this.forma.get('edad').updateValueAndValidity();

      this.forma.get('nombre').clearValidators();
      this.forma.get('nombre').updateValueAndValidity();

      this.forma.get('apellido').clearValidators();
      this.forma.get('apellido').updateValueAndValidity();
    }



  }

  submit(){
    this.forma.get('nombre').clearValidators();
    this.forma.get('nombre').updateValueAndValidity();

    console.log('forma-despues',this.forma.valid);

  }

}
