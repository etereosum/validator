import { ValidacionesPage } from './app.po';

describe('validaciones App', () => {
  let page: ValidacionesPage;

  beforeEach(() => {
    page = new ValidacionesPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
